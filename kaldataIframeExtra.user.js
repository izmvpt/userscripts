// ==UserScript==
// @name       	extraIframeOptions
// @match  	https://www.kaldata.com/*
// @require	https://code.jquery.com/jquery-3.3.1.min.js
// @require 	https://gist.githubusercontent.com/BrockA/2625891/raw/9c97aa67ff9c5d56be34a55ad6c18a314e5eb548/waitForKeyElements.js
// ==/UserScript==

waitForKeyElements('div.cke_contents iframe', options);

function options(iframe) {
	var src = iframe.attr('src');
	iframe.attr('src', src + '&showinfo=0&iv_load_policy=3&controls=0&rel=0');
}
