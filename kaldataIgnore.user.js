// ==UserScript==
// @name       	kaldataIgnoreUser
// @match  	https://www.kaldata.com/*
// @require	https://code.jquery.com/jquery-3.3.1.min.js
// @require 	https://gist.githubusercontent.com/BrockA/2625891/raw/9c97aa67ff9c5d56be34a55ad6c18a314e5eb548/waitForKeyElements.js
// @grant	GM.setValue
// @grant	GM.getValue
// ==/UserScript==

waitForKeyElements('article', ignore);
waitForKeyElements('li[data-role="activityItem"]', ignore_activity);

function ignore_activity(li) {
	/* Get userid from profile link in activity items */

	/* Find out if it's from thread, like, or other activity */
	if (li.hasClass('ipsStreamItem_contentBlock')) 	{
		var ips_class = 'ipsUserPhoto';
	} else if (li.hasClass('ipsStreamItem_actionBlock')) {
		ips_class = 'ipsType_break';
	}

	var profile = li.find('a.' + ips_class).attr('href');
	var nameid = profile.split('/')[5];
	var userid = nameid.split('-')[0].toString();
	var username = nameid.split('-')[1];
	async function b() {
		if (await GM.getValue(userid, false)) {
			//li.replaceWith(ignored(username));
			li.remove();
		}
	}
	b();
}

function ignored(who, id) {
	var div = $('<div></div>');
	var text = $('<p>Съдържание от блокиран потребител: </p>');
	var user = $('<span blocked_user>' + who + '</span>');
	div.css('padding-left', '3%');
	user.css({
		'color': '#3300ff85',
		'font-weight': '100',
	});
	user.click(function() {
		GM.deleteValue(id);
		alert(who + ' беше отблокиран.');
	});
	user.appendTo(text);
	text.appendTo(div);
	return $(div);
}


function ignore(article) {
	var data = JSON.parse(article.find('.ipsComment_content').attr('data-quotedata'));
	var ign_but = $('<li><a class="ipsFaded ipsFaded_more" href="javascript:void(0);">Игнорирай</a></li>');
	ign_but.css({
		'font-size':'9pt',
		'color':'#884041'
	});
	ign_but.prependTo(article.find('ul.ipsComment_tools'));
	var userid = data.userid.toString();
	ign_but.click(function() {
		GM.setValue(userid, true);
		alert(data.username + ' беше блокиран.');
	});
	async function b() {
		if (await GM.getValue(userid, false)) {
			article.replaceWith(ignored(data.username, userid));
		}
	}
	b();
	article.find('blockquote').each(async function() {
		var quoted_userid = $(this).attr('data-ipsquote-userid').toString();
		if (await GM.getValue(quoted_userid, false)) {
			$(this).find('.ipsQuote_contents').html('<i>(нежелано съдържание)</i>');	
		}
	});
}
