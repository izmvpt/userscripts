### What is this repository for? ###

JS userscripts for addons like greasemonkey.

### How do I get set up? ###

Install addon like Greasemonkey or Tampermonkey and copy these scripts into new ones.

### Contribution guidelines ###

No need for now.

### Who do I talk to? ###

bornofash at kaldata.com/forums

