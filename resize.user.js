// ==UserScript==
// @name       	kaldataResizer
// @description Resize elements on webpage.
// @match  	https://www.kaldata.com/forums/topic/*
// @exclude	https://www.kaldata.com/forums/topic/237456-%D1%80%D0%BE%D0%B4%D0%B8-%D1%81%D0%B5-%D0%BD%D0%BE%D0%B2%D0%BE-%D1%83%D1%82%D1%80%D0%BE-%D0%B4%D0%B0-%D1%81%D0%B8-%D0%BF%D0%BE%D0%B6%D0%B5%D0%BB%D0%B0%D0%B5%D0%BC%D1%87%D0%B0%D1%81%D1%82-%D0%BF%D0%BE%D1%80%D0%B5%D0%B4%D0%BD%D0%B0/*
// @require	https://code.jquery.com/jquery-3.3.1.min.js
// @require 	https://gist.githubusercontent.com/BrockA/2625891/raw/9c97aa67ff9c5d56be34a55ad6c18a314e5eb548/waitForKeyElements.js
// ==/UserScript==

function resize(item) {
	if (item.width() <= 300) return 0;
	item.css({
		'max-width': '100%',
		'width': '300px'
	});
};

waitForKeyElements("img", resize);

