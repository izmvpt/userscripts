// ==UserScript==
// @name kaldata video embed
// @description Try to auto embed video files.
// @author bornofash
// @match https://www.kaldata.com/forums/*
// @require	https://code.jquery.com/jquery-3.3.1.min.js
// @require https://greasyfork.org/scripts/2199-waitforkeyelements/code/waitForKeyElements.js?version=6349
// @version 0.0.5
// @namespace https://greasyfork.org/users/196421
// ==/UserScript==

(function() {
    'use strict';
    waitForKeyElements('div.cke_editable a[ipsnoembed]', embed);

    function embed(link) {
        if (link.closest('video').length !== 0) return 0;
        var video = put_in_video(link);
        if (video === 'stop') return 0;
        show_menu_for(video);
    }

    function put_in_video(source) {
        var src = source.attr('href');
        /* Embed only links containing file extention */
        if (src.indexOf('.mp4') !== -1) {
            var type = 'mp4';
        } else if (src.indexOf('.webm') !== -1) {
            type = 'webm';
        } else {
            return 'stop';
        }

        /* Create necessary tags */
        var video_tag = $('<video controls></video>');
        var source_tag = $('<source src=' + src + '#t=0.1 type="video/' + type + '">');
        /* Connect created tags */
        video_tag.append(source_tag);
        video_tag.append(src); // show link if video is not shown
        /* Style video */
       	video_tag.css({
            'max-width': '100%',
            'width':'auto',
        });

        source.before(video_tag);
        source.remove();

        return $(video_tag);
    }

    function show_menu_for(video) {
        /* Create necessary tags */

        var embedMessage = $('<div class="ipsHide ipsComposeArea_editorPaste" data-role="embedMessage"></div>');
        var ipsPad_half = $('<p class="ipsType_reset ipsPad_half"></p>');
        var close = $('<a class="ipsPos_right ipsType_normal ipsCursor_pointer ipsComposeArea_editorPasteSwitch">×</a>');
        var info = $('<i class="fa fa-cog" aria-hidden="true"></i>');
        /* Size */
        var small = $('<a class="ipsCursor_pointer ipsMarked_size" data-action="small">Малък</a>');
        var medium = $('<a class="ipsCursor_pointer ipsMarked_size" data-action="medium">Среден</a>');
        var big = $('<a class="ipsCursor_pointer ipsMarked_size" data-action="big">Голям</a>');
        /* Orientation */
        var left = $('<a class="ipsCursor_pointer ipsMarked_pos" data-action="left">Ляво</a>');
        var right = $('<a class="ipsCursor_pointer ipsMarked_pos" data-action="right">Дясно</a>');
        var nofloat = $('<a class="ipsCursor_pointer ipsMarked_pos" data-action="nofloat">Свободно</a>');

        /* Add functions to the links */
        function resize(size_tag, px) {
            size_tag.click(function() {
                $('.ipsMarked_size').css('filter', 'opacity(100%)');
                video.animate({
                    width: px
                }, 500);
                $(this).css('filter', 'opacity(30%)');
            });
        }
        resize(small, '320px');
        resize(medium, '640px');
        resize(big, '1000px');

        function align(pos_tag, pos) {
            pos_tag.click(function() {
                $('.ipsMarked_pos').css('filter', 'opacity(100%)');
                var margin_left, margin_right;
                if (pos === 'left') {
                    margin_right = '30px';
                    margin_left = '';
                } else if (pos === 'right') {
                    margin_right = '';
                    margin_left = '30px';
                } else {
                    margin_right = '';
                    margin_left = '';
                }
                video.css({
                    'float': pos,
                    'margin-left': margin_left,
                    'margin-right': margin_right,
                });
                $(this).css('filter', 'opacity(30%)');
            });
        }
        align(left, 'left');
        align(right, 'right');
        align(nofloat, 'none');

        close.click(function() {
            embedMessage.slideUp();
        });

        video.bind("DOMNodeRemoved", function () {
            embedMessage.slideUp();
        });

        /* Connect created tags */
        embedMessage.append(ipsPad_half);
        ipsPad_half.append(close);
        ipsPad_half.append(info);

        ipsPad_half.append(' Размер: ');
        ipsPad_half.append(small); small.after(' ');
        ipsPad_half.append(medium); medium.after(' ');
        ipsPad_half.append(big); big.after(' ');

        ipsPad_half.append(' Позиция: ');
        ipsPad_half.append(left); left.after(' ');
        ipsPad_half.append(right); right.after(' ');
        ipsPad_half.append(nofloat); nofloat.after(' ');

        video.closest('[data-role="mainEditorArea"]').after(embedMessage);
        embedMessage.slideDown();
    }
})();
