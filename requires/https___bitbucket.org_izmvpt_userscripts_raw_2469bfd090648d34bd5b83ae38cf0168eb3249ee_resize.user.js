// ==UserScript==
// @name       	kaldataResizer
// @description Resize elements on webpage.
// @match  	https://www.kaldata.com/forums/topic/*
// @require	https://code.jquery.com/jquery-3.3.1.min.js
// @require 	https://gist.githubusercontent.com/BrockA/2625891/raw/9c97aa67ff9c5d56be34a55ad6c18a314e5eb548/waitForKeyElements.js
// ==/UserScript==

function resize(item) {
	item.each(function () {
		$(this).css('max-width', '100%');
		$(this).css('width', '300px');
	});
};

waitForKeyElements("img.ipsImage", resize, false);

