// ==UserScript==
// @name    kaldataSpoiler
// @match  	https://www.kaldata.com/forums/*
// @require	https://code.jquery.com/jquery-3.3.1.min.js
// @require https://gist.githubusercontent.com/BrockA/2625891/raw/9c97aa67ff9c5d56be34a55ad6c18a314e5eb548/waitForKeyElements.js
// ==/UserScript==

waitForKeyElements('.cke_toolgroup', add_button);

function create_button(pos, cmd, title) {
	var button, icon;
	button = $('<a></a>');
	button.attr({
		'class':'cke_button cke_button_off',
		'title': title,
		'role':'button'
	});
	icon = $('<span></span>');
	icon.attr({
		'class':'cke_button_icon',
	});
	icon.css({
		"background-image": "url('//www.kaldata.com/forums/applications/core/interface/ckeditor/ckeditor/plugins/icons.png?t=I6QJ')",
		"background-position": "0 " + pos,
		"background-size": "auto"
	});
	button.click(function() {
		var currentInstance = unsafeWindow.CKEDITOR.currentInstance.name;
		unsafeWindow.CKEDITOR.instances[currentInstance].execCommand(cmd);
	});
	icon.appendTo(button);
	return $(button);
}

function add_button(toolbar) {
	/* create_button('icon_position', 'cke_command') */
	toolbar.append(create_button('-673px', 'ipsspoiler', 'Спойлер'));
	toolbar.append(create_button('-698px', 'ipspage', 'Черта'));
	toolbar.append(create_button('-913px', 'source', 'Редактирай HTML кода'));
}
