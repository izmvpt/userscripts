// ==UserScript==
// @name       	kaldataVideoLink
// @description Show link to each video above them..
// @match  	https://www.kaldata.com/forums/*
// @require	https://code.jquery.com/jquery-3.3.1.min.js
// @require 	https://gist.githubusercontent.com/BrockA/2625891/raw/9c97aa67ff9c5d56be34a55ad6c18a314e5eb548/waitForKeyElements.js
// ==/UserScript==

function frame_url(str) {
	if (str === undefined) return 0;
	// Youtube links are short
	// so we don't need to cut them
	if(str.indexOf("youtube.com") > 0) {
		return str;
	}
	// Shorten url by cutting out kaldata info
	parts = str.split("url=");
	return parts[parts.length - 1];
}

function link_frames(frame) {
	// Get iframe parent
	var parent = frame.closest("[class^='ipsEmbedded']");

	// Get information from iframe and put it in a link
	var src = frame.attr("src");
	var title = "Изваден от видеото линк за специални случаи.";
	var a = $('<a></a>');
	a.attr({
		'title': title, 
		'href': src
	});
	a.text(frame_url(src));

	// Prepend iframe link
	// if it's not in comment box
	if (frame.closest('.cke_contents').length === 0)
		a.insertBefore(parent);

	// Resize iframe div parent
	parent.css({
		"width":"500px",
		"max-width":"100%",
	});
};

waitForKeyElements("iframe", link_frames, false);


