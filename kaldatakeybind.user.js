// ==UserScript==
// @name       	kaldatAccessKeys
// @description Add some more access keys to kaldata.com/forums
// @match  	https://www.kaldata.com/*
// @require	https://code.jquery.com/jquery-3.3.1.min.js
// @require 	https://gist.githubusercontent.com/BrockA/2625891/raw/9c97aa67ff9c5d56be34a55ad6c18a314e5eb548/waitForKeyElements.js
// ==/UserScript==

var key = {
	compose:'p', 
	load: 	'n',
};

$(function() {
	// Фокусирай полето за създаване на пост
	$('div.ipsComposeArea_dummy').attr('accesskey', key.compose);
	// Зареди новите съобщения в потока с последна активност
	function loadMore (load) { load.attr('accesskey', key.load); };
	waitForKeyElements('li.ipsStreamItem_loadMore', loadMore);
	waitForKeyElements('a[data-action="loadNewPosts"]', loadMore);
});

