// ==UserScript==
// @name       	kaldatRandomReason
// @description Pick random reason for editing a post
// @match  	https://www.kaldata.com/*
// @require	https://code.jquery.com/jquery-3.3.1.min.js
// @require 	https://gist.githubusercontent.com/BrockA/2625891/raw/9c97aa67ff9c5d56be34a55ad6c18a314e5eb548/waitForKeyElements.js
// @grant	GM.xmlhttprequest
// ==/UserScript==

waitForKeyElements('input[name="comment_edit_reason"]', reason);

var phrases = [
	'Конвергентен конкомитентен страбизъм',
	'С една човка само трудно се пише без грешки',
	'Изкуствен интелект в прогрес',
	'Един червей ме разсейва. Един друг червей.',
	'Забравих да напсувам някого',
	'Друсан съм с хлебен мухал',
	'Един файл беше по-голям от допустимото и беше пропуснати',
	'Синестезията се обажда отново',
	'Помниш ли бухала ^o,o^',
	'Не се изпонаприказвах',
	'https://i.imgur.com/k7bAowx.png',
	'https://i.imgur.com/k7bAowx.png',
	'https://i.imgur.com/k7bAowx.png',
	'https://i.imgur.com/k7bAowx.png',
	'https://i.imgur.com/k7bAowx.png',
	'https://i.imgur.com/k7bAowx.png',

];

function reason(input) {
	var phrase = phrases[Math.floor(Math.random() * phrases.length)];
	/* Change input value */
	input.val(phrase);
	/* Also check the box */
	$('input[name="comment_log_edit_checkbox"]').attr('checked', 'checked'); 
};
